const arrayOfObjects = require("./data");
function CityCountry(arrayOfObjects) {
    let city_country = []
    if (Array.isArray(arrayOfObjects)) {
        for (let individual of arrayOfObjects) {
            city_country.push({
                name: individual["name"], city: individual["city"], country: individual["country"]
            }
            )
        }
    }
    return city_country
}
console.log(CityCountry(arrayOfObjects));