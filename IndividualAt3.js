const arrayOfObjects = require("./data");

function get_individual_3(arrayOfObjects) {
    if (arrayOfObjects.length === 0) {
        return `given data is empty`
    } else if (Array.isArray(arrayOfObjects)) {
        let name = arrayOfObjects[3].name;
        let city = arrayOfObjects[3].city;
        return `name of individual at index 3 is ${name} and city is ${city}`;
    }
    return "invalid input"
}
console.log(get_individual_3("arrayOfObjects"));
