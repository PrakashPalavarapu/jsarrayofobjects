const arrayOfObjects = require("./data")
const _ = require("underscore")

function get_hoobies_ByAge(arrayOfObjects, age = 30) {
    hobbies = []
    if (Array.isArray(arrayOfObjects) && (typeof age === "number")) {
        for (let individual of arrayOfObjects) {
            if (individual["age"] == age) {
                hobbies.push(individual["hobbies"]);
            }
        }
    }
    return _.flatten(hobbies);
}

console.log(get_hoobies_ByAge(arrayOfObjects, 30));
