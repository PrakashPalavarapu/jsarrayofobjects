const arrayOfObjects = require("./data");
function get_data_age25(arrayOfObjects) {
   let name_email = {};
   if (Array.isArray(arrayOfObjects)) {
      for (let individual of arrayOfObjects) {
         if (individual["age"] === 25) {
            name_email[individual.name] = individual["email"];
         }
      }
   }
   return name_email;
}
console.log(get_data_age25(arrayOfObjects));