const arrayOfObjects = require("./data");
function first_hobby(arrayOfObjects) {
   let each_first_hobby = {};
   if (Array.isArray(arrayOfObjects)) {
      for (let individual of arrayOfObjects) {
         if(individual["hobbies"].length > 0){
            each_first_hobby[individual.name] = individual["hobbies"][0]
         };
      }
   }
   return each_first_hobby;
}

console.log(first_hobby(arrayOfObjects));
