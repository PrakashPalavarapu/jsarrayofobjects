const arrayOfObjects = require("./data");
function get_AusisStudents(arrayOfObjects) {
    let AusisStudents = [];
    if (Array.isArray(arrayOfObjects)) {
        for (let individual of arrayOfObjects) {
            if (individual.isStudent && individual.country === "Australia") {
                AusisStudents.push(individual["name"]);
            }
        }
    }
    return AusisStudents;
}
console.log(get_AusisStudents(arrayOfObjects));
